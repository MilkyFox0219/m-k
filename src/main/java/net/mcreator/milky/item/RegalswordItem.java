
package net.mcreator.milky.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.SwordItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;

import net.mcreator.milky.itemgroup.MilkynkonxItemGroup;
import net.mcreator.milky.MilkyModElements;

@MilkyModElements.ModElement.Tag
public class RegalswordItem extends MilkyModElements.ModElement {
	@ObjectHolder("milky:regalsword")
	public static final Item block = null;

	public RegalswordItem(MilkyModElements instance) {
		super(instance, 31);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new SwordItem(new IItemTier() {
			public int getMaxUses() {
				return 100;
			}

			public float getEfficiency() {
				return 4f;
			}

			public float getAttackDamage() {
				return 7f;
			}

			public int getHarvestLevel() {
				return 1;
			}

			public int getEnchantability() {
				return 2;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(RegalgemItem.block));
			}
		}, 3, -2.2f, new Item.Properties().group(MilkynkonxItemGroup.tab).isImmuneToFire()) {
		}.setRegistryName("regalsword"));
	}
}
