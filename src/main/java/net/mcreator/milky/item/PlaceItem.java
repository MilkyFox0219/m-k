
package net.mcreator.milky.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.util.ResourceLocation;
import net.minecraft.item.Rarity;
import net.minecraft.item.MusicDiscItem;
import net.minecraft.item.Item;

import net.mcreator.milky.itemgroup.MilkynkonxItemGroup;
import net.mcreator.milky.MilkyModElements;

@MilkyModElements.ModElement.Tag
public class PlaceItem extends MilkyModElements.ModElement {
	@ObjectHolder("milky:place")
	public static final Item block = null;

	public PlaceItem(MilkyModElements instance) {
		super(instance, 47);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new MusicDiscItemCustom());
	}

	public static class MusicDiscItemCustom extends MusicDiscItem {
		public MusicDiscItemCustom() {
			super(0, MilkyModElements.sounds.get(new ResourceLocation("milky:place")),
					new Item.Properties().group(MilkynkonxItemGroup.tab).maxStackSize(1).rarity(Rarity.RARE));
			setRegistryName("place");
		}
	}
}
