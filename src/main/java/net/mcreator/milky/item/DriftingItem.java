
package net.mcreator.milky.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.world.World;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.ResourceLocation;
import net.minecraft.item.Rarity;
import net.minecraft.item.MusicDiscItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.client.util.ITooltipFlag;

import net.mcreator.milky.itemgroup.MilkynkonxItemGroup;
import net.mcreator.milky.MilkyModElements;

import java.util.List;

@MilkyModElements.ModElement.Tag
public class DriftingItem extends MilkyModElements.ModElement {
	@ObjectHolder("milky:drifting")
	public static final Item block = null;

	public DriftingItem(MilkyModElements instance) {
		super(instance, 99);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new MusicDiscItemCustom());
	}

	public static class MusicDiscItemCustom extends MusicDiscItem {
		public MusicDiscItemCustom() {
			super(0, MilkyModElements.sounds.get(new ResourceLocation("milky:drifting")),
					new Item.Properties().group(MilkynkonxItemGroup.tab).maxStackSize(1).rarity(Rarity.RARE));
			setRegistryName("drifting");
		}

		@Override
		public void addInformation(ItemStack itemstack, World world, List<ITextComponent> list, ITooltipFlag flag) {
			super.addInformation(itemstack, world, list, flag);
			list.add(new StringTextComponent("\"We see you still in the Nihlanths chamber\""));
			list.add(new StringTextComponent("composed by Konxovar"));
		}
	}
}
