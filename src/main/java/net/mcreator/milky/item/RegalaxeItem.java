
package net.mcreator.milky.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;
import net.minecraft.item.AxeItem;

import net.mcreator.milky.itemgroup.MilkynkonxItemGroup;
import net.mcreator.milky.MilkyModElements;

@MilkyModElements.ModElement.Tag
public class RegalaxeItem extends MilkyModElements.ModElement {
	@ObjectHolder("milky:regalaxe")
	public static final Item block = null;

	public RegalaxeItem(MilkyModElements instance) {
		super(instance, 85);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new AxeItem(new IItemTier() {
			public int getMaxUses() {
				return 3000;
			}

			public float getEfficiency() {
				return 8f;
			}

			public float getAttackDamage() {
				return 3.5f;
			}

			public int getHarvestLevel() {
				return 5;
			}

			public int getEnchantability() {
				return 3;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(RegalgemItem.block));
			}
		}, 1, -3.2f, new Item.Properties().group(MilkynkonxItemGroup.tab)) {
		}.setRegistryName("regalaxe"));
	}
}
