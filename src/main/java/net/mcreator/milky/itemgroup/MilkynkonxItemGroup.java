
package net.mcreator.milky.itemgroup;

import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;

import net.mcreator.milky.item.MilkyNKonxovarItem;
import net.mcreator.milky.MilkyModElements;

@MilkyModElements.ModElement.Tag
public class MilkynkonxItemGroup extends MilkyModElements.ModElement {
	public MilkynkonxItemGroup(MilkyModElements instance) {
		super(instance, 3);
	}

	@Override
	public void initElements() {
		tab = new ItemGroup("tabmilkynkonx") {
			@OnlyIn(Dist.CLIENT)
			@Override
			public ItemStack createIcon() {
				return new ItemStack(MilkyNKonxovarItem.block);
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}

	public static ItemGroup tab;
}
